import openai
import json

with open("text/bsb.json") as f:
    bible_data = [verse for verse in json.load(f) if verse["book"] == "Matthew" and verse["chapter"] == 17]

for verse in bible_data:
    sentences = [verse["text"]]
    try:
        embeddings = openai.Embedding.create(input=sentences, model="text-embedding-ada-002")
        print(len(embeddings["data"][0]["embedding"]))
    except:
        print("Error")
        print(verse)
