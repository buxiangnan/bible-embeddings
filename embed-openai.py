import argparse
import json
import os
import openai
from itertools import groupby
from tenacity import retry, wait_exponential

arg_parser = argparse.ArgumentParser(prog="embed-openai", description="Creates embeddings of Bible verses using OpenAI API")
arg_parser.add_argument("translation", help="Located at text/<translation>.json")

args = arg_parser.parse_args()

with open(f"text/{args.translation}.json") as f:
    bible_data = [verse for verse in json.load(f) if len(verse["text"]) > 0]

grouped_bible_data = groupby(bible_data, lambda item: (item["book"], item["chapter"]))

model_name = "text-embedding-ada-002"

@retry(wait=wait_exponential(multiplier=2, min=2, max=30))
def get_embeddings(sentences):
    return openai.Embedding.create(input=sentences, model=model_name)["data"]

for (book, chapter), grouped in grouped_bible_data:
    print(f"Embedding {book} {chapter}")
    verses = list(grouped)
    data = []
    base_path = os.path.join("embeddings", book, str(chapter).zfill(3))
    os.makedirs(base_path, exist_ok=True)
    sentences = [verse["text"] for verse in verses]
    embeddings = get_embeddings(sentences)
    for i, verse in enumerate(verses):
        data.append({ "book": verse["book"], "chapter": verse["chapter"], "verse": verse["verse"], "embedding": embeddings[i]["embedding"] })
    with open(os.path.join(base_path, f"{args.translation}-{model_name}.json"), "w", encoding='utf-8') as f:
        json.dump(data, f, indent=2)
