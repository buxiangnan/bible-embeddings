import argparse
import json
import os
from itertools import groupby
from sentence_transformers import SentenceTransformer

arg_parser = argparse.ArgumentParser(prog="embed", description="Creates embeddings of Bible verses")
arg_parser.add_argument("translation", help="Located at text/<translation>.json")
arg_parser.add_argument("model", help="SentenceTransformer model")

args = arg_parser.parse_args()

with open(f"text/{args.translation}.json") as f:
    bible_data = json.load(f)

grouped_bible_data = groupby(bible_data, lambda item: (item["book"], item["chapter"]))

model = SentenceTransformer(args.model)
model_name = args.model.split("/")[-1]

for (book, chapter), grouped in grouped_bible_data:
    print(f"Embedding {book} {chapter}")
    verses = list(grouped)
    data = []
    base_path = os.path.join("embeddings", book, str(chapter).zfill(3))
    os.makedirs(base_path, exist_ok=True)
    sentences = [verse["text"] for verse in verses]
    embeddings = model.encode(sentences, normalize_embeddings=True)
    for i, verse in enumerate(verses):
        data.append({ "book": verse["book"], "chapter": verse["chapter"], "verse": verse["verse"], "embedding": embeddings[i].tolist() })
    with open(os.path.join(base_path, f"{args.translation}-{model_name}.json"), "w", encoding='utf-8') as f:
        json.dump(data, f, indent=2)
