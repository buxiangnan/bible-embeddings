download-bsb:
  curl https://bereanbible.com/bsb.txt | iconv -f windows-1252 -t utf-8 > text/bsb.txt

install-deps:
  pip install -r requirements.txt

embed translation model:
  python embed.py {{translation}} {{model}}

embed-openai translation:
  python embed-openai.py {{translation}}

query translation model query:
  python query.py {{translation}} {{model}} "{{query}}"

query-openai translation query:
  python query-openai.py {{translation}} "{{query}}"

size translation model:
  find embeddings -type f -name '{{translation}}-{{model}}.json' -exec stat -c "%s" {} + | awk '{ total += $1 } END { print total / (1000 * 1000) }'
