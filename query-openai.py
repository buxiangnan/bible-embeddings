import argparse
import json
import os
import openai
import numpy as np

arg_parser = argparse.ArgumentParser(prog="query", description="Queries embeddings of Bible verses")
arg_parser.add_argument("translation", help="Located at text/<translation>.json")
arg_parser.add_argument("query", help="Text to search for")

args = arg_parser.parse_args()

model_name = "text-embedding-ada-002"

verses = []
for root, dirs, files in os.walk("embeddings"):
    for filename in files:
        if filename == f"{args.translation}-{model_name}.json":
            file_path = os.path.join(root, filename)
            with open(file_path, "r") as json_file:
                verses.extend(json.load(json_file))

embedding = np.array(openai.Embedding.create(input=[args.query], model=model_name)["data"][0]["embedding"])

results = []
for verse in verses:
    score = verse["embedding"] @ embedding
    item = { "book": verse["book"], "chapter": verse["chapter"], "verse": verse["verse"] }
    results.append((score, item))

results.sort(key=lambda res: res[0], reverse=True)
print(json.dumps(results[:10], indent=2))
