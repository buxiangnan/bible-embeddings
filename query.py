import argparse
import json
import os
from sentence_transformers import SentenceTransformer

arg_parser = argparse.ArgumentParser(prog="query", description="Queries embeddings of Bible verses")
arg_parser.add_argument("translation", help="Located at text/<translation>.json")
arg_parser.add_argument("model", help="SentenceTransformer model")
arg_parser.add_argument("query", help="Text to search for")

args = arg_parser.parse_args()

verses = []
for root, dirs, files in os.walk("embeddings"):
    for filename in files:
        model_name = args.model.split("/")[-1]
        if filename == f"{args.translation}-{model_name}.json":
            file_path = os.path.join(root, filename)
            with open(file_path, "r") as json_file:
                verses.extend(json.load(json_file))

model = SentenceTransformer(args.model)
embedding = model.encode([args.query], normalize_embeddings=True)[0]

results = []
for verse in verses:
    score = verse["embedding"] @ embedding
    item = { "book": verse["book"], "chapter": verse["chapter"], "verse": verse["verse"] }
    results.append((score, item))

results.sort(key=lambda res: res[0], reverse=True)
# print(json.dumps(results[:3], indent=2))
formatted = [f"{item[1]['book']} {item[1]['chapter']}:{item[1]['verse']} ({item[0]:.4f})" for item in results[:3]]
print(", ".join(formatted))
