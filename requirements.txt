numpy==1.25.2
openai==0.27.9
sentence-transformers==2.2.2
tenacity==8.2.3
